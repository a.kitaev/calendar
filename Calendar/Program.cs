﻿using System;

namespace Calendar {
    class Program {
        static void Main(string[] args) {
            var dateStr = Console.ReadLine();
            DateTime dateTime;

            if (DateTime.TryParse(dateStr, out dateTime)) {
                PrintCalendar(dateTime);
            } else {
                Console.WriteLine("Could not recognize date");
            }
        }

        private static void PrintCalendar(DateTime dateTime) {
            PrintDaysOfWeek();
            var firstDayOfMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
            var lastDayOfMonth = new DateTime(dateTime.Year, dateTime.Month,
                DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
            PrintCalendar(firstDayOfMonth, lastDayOfMonth);
            PrintNumberOfWorkingDays(firstDayOfMonth, lastDayOfMonth);
        }

        private static void PrintDaysOfWeek() {
            Console.WriteLine("Mo\tTu\tWe\tTh\tFr\tSa\tSu");
        }

        private static void PrintCalendar(DateTime firstDayOfMonth, DateTime lastDayOfMonth) {
            int dayNum = -1 * (int) firstDayOfMonth.DayOfWeek + 2;
            for (int i = 0; i < 5; i++) {
                for (DayOfWeek j = 0; j < (DayOfWeek) 7; j++) {
                    String symbol;
                    if (dayNum < 1 || dayNum > lastDayOfMonth.Day) {
                        symbol = ".";
                    } else {
                        symbol = dayNum.ToString();
                    }

                    Console.Write(symbol + "\t");
                    dayNum++;
                }

                Console.WriteLine();
            }
        }

        private static void PrintNumberOfWorkingDays(DateTime firstDayOfMonth, DateTime lastDayOfMonth) {
            int count = 0;
            for (DateTime i = firstDayOfMonth; i <= lastDayOfMonth; i = i.AddDays(1)) {
                if (!(i.DayOfWeek == DayOfWeek.Saturday || i.DayOfWeek == DayOfWeek.Sunday)) {
                    count++;
                }
            }
            Console.WriteLine("Number of working days = " + count);
        }
    }
}